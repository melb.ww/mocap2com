function [all_Names] = f_pickFolderFromFolder(folder_input,keyWord)
folders_Cam=dir(folder_input);
all_names={folders_Cam.name};
isNotFolder=[folders_Cam.isdir];
all_names_picked=all_names(isNotFolder)';
indx_pickCam=contains(all_names_picked, keyWord,'IgnoreCase',true);
all_Names=all_names_picked(indx_pickCam);
end

