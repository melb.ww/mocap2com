function data_remove_shortOnes=f_remove_shortEMG(data_1col)
% data_1col=(MVC_EMG_raw_EMG_MVC_all.EMG_proc(:,1));
TH=max(data_1col)*0.05;
indx_0=data_1col<TH;
indx_1=~indx_0;

steP_diff=diff(indx_1);

goup=find(steP_diff==1);
godown=find(steP_diff==-1);

if godown(1)<goup(1)
    godown(1)=[];
end

goup=goup(1:length(godown));
gap=godown-goup;

TH_step=0.6e4;

gap_index=(gap>TH_step);

goupTH=goup(gap_index);
godownTH=godown(gap_index);

indx_pick=[];
for n=1:length(goupTH)
    indx_pick=[indx_pick,goupTH(n):godownTH(n)];
end
data_remove_shortOnes=zeros(size(data_1col));
data_remove_shortOnes(indx_pick)=data_1col(indx_pick);
end
