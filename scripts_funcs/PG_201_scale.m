ccc
folder_c3d=f_get_path_same_level('data_lfs\c3d');
trialFolderSet=f_pickFolderFromFolder(folder_c3d,'subject');
%%
ct=0;
SetCondition={'barefoot','AO','AP','AR','BO','BP','BR','CO','CP','CR'};

for n_subj=1%1:length(trialFolderSet)
    
    Subject.name=trialFolderSet{n_subj}
    ct=ct+1;
    if 0
        
        name_set{ct,1}=imp.textdata{n_subj+1,1};
        %         ind=find(contains(imp_subjdata.textdata,name_set{ct}));
        if imp.data(n_subj,2)==1
            name_set{ct,2}='l';
        else
            name_set{ct,2}='r';
        end
        
        
    end
    Subject.mass=60;
    Subject.height=1.6;
    Subject.LR='R';
    
    
    for n_condition=1:10 %=========================
        Condition=SetCondition{n_condition};
        if n_condition==1
            Subject.flag_shod=0;
        else
            Subject.flag_shod=1;
        end
        if 0 % copy repo
            folder_nexus='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2020_product_testing\data_collected\Asics_product_testing\Pilot\';
            C_modeltool.f_S_copyNexusC3D_2_Repo(folder_nexus,'',Subject,Condition);
            
        end
        
        if 1 % size scale
            folder_conditon=fullfile(folder_c3d,Subject.name,Condition);
            cali_file=f_pickFromFolder(folder_conditon,'cali');
            if isempty(cali_file)
                continue
            end
            
            [~,name_temp,~] = fileparts(cali_file{1});
            %             C_modeltool.f_S_repo_C3D(Subject,Condition);
            if 0
                x = input('cali_file= ','s');
                static_trial=C_modeltool_v200(Subject,Condition,[x],1);
            else
                static_trial=C_modeltool_v200(Subject,Condition,name_temp,1);
            end
            %             static_trial.f_updateBodyWeight_fromCali();
            1;
            if 1
                static_trial.f_writeTRCGRF_OS(1);
                
                if 0
                    static_trial.f_setup_scale_API('GUI');
                    x = input(' ... ','s');% static_trial.f_notificatin();
                else
                    static_trial.f_setup_scale_API('API');
                end
                
            end
            
        end
        
        if 1 % Fmax scale
%             static_trial=C_modeltool(Subject,Condition,'Cal 01',0);
            static_trial.f_setup_scale_strengh(1.5);
            %         static_trial.f_setup_scale_strengh_temp(6.82);
        end
    end
    
end


