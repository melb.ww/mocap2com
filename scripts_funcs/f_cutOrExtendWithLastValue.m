function [output] = f_cutOrExtendWithLastValue(input,outLength)
if length(input)>=outLength
    output=input(1:outLength);
else
    output=input;
    output(end+1:outLength)=input(end);
end

