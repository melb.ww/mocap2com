function p=f_SO_obj_EMGinformed(a,EMG_temp,EMGs_vs_osimMuscIndx)

p1=sum(a.*a);


indx_EMG=EMGs_vs_osimMuscIndx(:,1);
indx_muscle=EMGs_vs_osimMuscIndx(:,2);
gap_temp=(a(indx_muscle)-EMG_temp(indx_EMG));
p2=sum(gap_temp.*gap_temp);

p=3*p1+p2;
end