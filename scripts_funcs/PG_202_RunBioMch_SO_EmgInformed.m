ccc
folder_c3d=f_get_path_same_level('data_lfs\c3d');
trialFolderSet=f_pickFolderFromFolder(folder_c3d,'subject');
imp_subjectData=load('..\docs\Subject_details_realTest_full.mat');
%%

ct_error=0;

SetCondition={'barefoot','AO','AP','AR','BO','BP','BR','CO','CP','CR'};
for n_subj=1%1:length(trialFolderSet)
    
    
    Subject.name=trialFolderSet{n_subj};
    Subject.mass=0;
    Subject.height=0;
    Subject.LR='R';
    
    for n_condition=1%:10
        Condition=SetCondition{n_condition};
        if n_condition==1
            Subject.flag_shod=0;
        else
            Subject.flag_shod=1;
        end
        
        
        C_modeltool_v200.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool_v200.f_S_repo_C3D_trials(Subject,Condition);
        
        
        pick_keyword='drop';%pick_keyword='drop';
        name_set_c3d_picked=name_set_c3d(contains(name_set_c3d,pick_keyword,'IgnoreCase',true))';
        
        ct_k=0;
        for k=1:length(name_set_c3d_picked)
            ct_k=ct_k+1;
            %             try
            [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
            Lb2=C_modeltool_v200(Subject,Condition,temp_trial,1);
            
            if 0%exist(Lb2.file_res_KneeForce_GUI,'file')
                continue
            end
            if 1
                Lb2.setup_forceplate_LR={{'r','3'}};
            else
                Lb2.f_setup_forceplate_LR_from_c3dname();
            end
            if 1 %IK, ID
                if ~exist(Lb2.file_mot,'file') % Run IK
                    Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
                    Lb2.f_trim_mot_by_GRF()
                    Lb2.f_setup_IK_API(0);
                end
                if 0% Check IK erros
                    error_mean=Lb2.f_get_IK_error();
                    error_mean_max(n_subj,ct_k)=error_mean(2);
                    error_mean_rms(n_subj,ct_k)=error_mean(1);
                end
                
                Lb2.f_setup_ExtLoads(0);%flag_winopen_extLoadxml, Lb2.f_setup_ExtLoads(1)
                
                if ~exist(Lb2.file_res_ID, 'file') % Run Inverse Dynamics tool
                    Lb2.f_load_model();
                    Lb2.f_load_res_mot();
                    Lb2.f_setup_run_ID(6);
                    Lb2.f_load_res_ID();
                    
                    Lb2.f_load_res_ID_interest();
                end
            end
            
            
            if 0 %MOCO playground
                Lb2.f_load_model;
                %             Lb2.OS.model
                
                import org.opensim.modeling.*;
                
                
                modelProcessor = ModelProcessor(Lb2.OS.model);
                
                
                
                modelProcessor.append(ModOpAddExternalLoads(Lb2.file_setup_ExtLoads));
                % Update the muscles: ignore tendon compliance and passive forces, replace
                %                 muscle types with the DeGrooteFregly2016Muscle type, and scale the width
                %                 of the active force length curve.
                modelProcessor.append(ModOpIgnoreTendonCompliance());
                modelProcessor.append(ModOpReplaceMusclesWithDeGrooteFregly2016());
                modelProcessor.append(ModOpIgnorePassiveFiberForcesDGF());
                modelProcessor.append(ModOpScaleActiveFiberForceCurveWidthDGF(1.5));
                % Add a set a weak reserves to the sagittal-plane joints in the model.
                modelProcessor.append(ModOpAddReserves(1.0));
                
                %
                inverse = MocoInverse();
                inverse.setModel(modelProcessor);
                
                % Part 1c: Create a TableProcessor using the coordinates file from inverse
                % kinematics.
                
                coordinates = TableProcessor(Lb2.file_mot);
                %                 coordinates.append(TabOpLowPassFilter(6));
                coordinates.append(TabOpUseAbsoluteStateNames());
                
                % Part 1d: Set the kinematics reference for MocoInverse using the
                % TableProcessor we just created.
                inverse.setKinematics(coordinates);
                inverse.set_kinematics_allow_extra_columns(true);
                
                % Part 1e: Provide the solver settings: initial and final time, the mesh
                % interval, and the constraint and convergence tolerances.
                inverse.set_initial_time(0.1);
                inverse.set_final_time(0.2);
                inverse.set_mesh_interval(0.04);
                inverse.set_constraint_tolerance(1e-3);
                inverse.set_convergence_tolerance(1e-3);
                
                if 1%~exist('effortSolution.sto', 'file')
                    % Part 1f: Solve the problem!
                    inverseSolution = inverse.solve();
                    solution = inverseSolution.getMocoSolution();
                    solution.write('effortSolution.sto');
                end
                
            end
            if 0 % Run SO using the OpenSim built-in SO tool
                %Lb2.f_setup_reserve_actuator();
                Lb2.f_setup_run_SO_GUI();
                
                % GRF $$$$$$$$
                Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
                Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
                Lb2.f_makefile_Knee_force_GUI();
                1;
            end
            
            
            if 1 % EMG-informed SO
                Lb2.f_load_res_ID_interest();
                if ~exist(Lb2.file_state_musc, 'file') % make muscle state files
                    Lb2.f_get_MA_Fvel();
                else
                    Lb2.f_load_MA_Fvel();
                end
                
                if ~exist(Lb2.file_res_SO, 'file')
                    Lb2.f_setup_run_SO(1);  % Lb2.f_setup_run_SO(0);
                    Lb2.f_save_res_SO();
                end
                
                if 0 % Load EMGs, if skipped, will be loaded inside f_setup_run_SO_EMGinformed
                    Lb2.f_load_EMG_trial();%Lb2.f_load_EMG_trial_MVC();
                    if 0 % plot loaded EMG
                        Lb2.f_fig_subplot_8EMGs('EMG')
                    end
                end
                
                
                if ~exist(Lb2.file_res_SO_emgInformed, 'file')
                    Lb2.f_setup_run_SO_EMGinformed(1);% Lb2.f_setup_run_SO_EMGinformed(0);
                    Lb2.f_save_res_SO_emgInformed();
                    
                    
                end
                
                
                if 0  %Plot SO results
                    Lb2.f_load_res_SO();
                    Lb2.f_load_res_SO_emgInformed();
                    
                    figure;
                    
                    if 1 % plot my own SO results
                        plot_config.flag_GRF_event=0;
                        plot_config.flag_selected_muscs=1;
                        plot_config.flag_EMG=0;
                        plot_config.Ylim=[0 1];
                        Lb2.f_fig_subplot_res_a(Lb2.res_SO,plot_config);% flag_GRF_event=1;
                    end
                    
                    
                    if 1 %Plot EMG-informed SO results
                        plot_config.flag_GRF_event=1;
                        plot_config.flag_selected_muscs=1;
                        plot_config.flag_EMG=1;
                        plot_config.Ylim=[0 1];
                        Lb2.f_fig_subplot_res_a(Lb2.res_SO_emgInformed,plot_config);% flag_GRF_event=1;
                    end
                    
                    if 1 %plot OpenSim build in SO results
                        Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
                    end
                    if 0 % Compare active vs passive force
                        plot_config.flag_GRF_event=0;
                        plot_config.flag_selected_muscs=1;
                        plot_config.flag_EMG=0;
                        plot_config.Ylim=[0 5000];
                        
                        fig_num=10;
                        figure(fig_num);Lb2.f_fig_subplot_res_OS_options(Lb2.res_SO_emgInformed,plot_config,'F_pas');% flag_GRF_event=1;
                        figure(fig_num);Lb2.f_fig_subplot_res_OS_options(Lb2.res_SO_emgInformed,plot_config,'F_act');% flag_GRF_event=1;
                        figure(fig_num);Lb2.f_fig_subplot_res_OS_options(Lb2.res_SO_emgInformed,plot_config,'Fm');% flag_GRF_event=1;
                    end
                    
                end
                
            end
        end
    end
end

%%
if 0 % Plots
    Lb2.f_GUI_c3d;
    Lb2.f_GUI_mot;
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(0,0); %F
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
    Lb2.f_fig_subplot_res_Knee_child(0);
    Lb2.f_fig_subplot_T_interest_ID(0);
    %     Lb2.f_fig_subplot_res_OpenSim_SO_a(0,0);
end
