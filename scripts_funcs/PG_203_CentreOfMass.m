ccc
folder_c3d=f_get_path_same_level('data_lfs\c3d');
trialFolderSet=f_pickFolderFromFolder(folder_c3d,'subject');

%%

ct_error=0;

SetCondition={'barefoot','AO','AP','AR','BO','BP','BR','CO','CP','CR'};
for n_subj=1%1:length(trialFolderSet)
    
    
    Subject.name=trialFolderSet{n_subj};
    Subject.mass=60;
    Subject.height=1.6;
    Subject.LR='R';
    
    for n_condition=1%:10
        Condition=SetCondition{n_condition};
        if n_condition==1
            Subject.flag_shod=0;
        else
            Subject.flag_shod=1;
        end
        
        
        C_modeltool_v200.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool_v200.f_S_repo_C3D_trials(Subject,Condition);
        
        
        pick_keyword='drop';%pick_keyword='drop';
        name_set_c3d_picked=name_set_c3d(contains(name_set_c3d,pick_keyword,'IgnoreCase',true))';
        
        ct_k=0;
        for k=1:length(name_set_c3d_picked)
            ct_k=ct_k+1;
  
            [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
            Lb2=C_modeltool_v200(Subject,Condition,temp_trial,1);
            

            
            if ~exist(Lb2.file_mot,'file') % Run Inverse Kinematics
                Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
                %                     Lb2.f_trim_mot_by_GRF()
                Lb2.f_setup_IK_API(0);
            end
            
            if ~exist(Lb2.folder_res_CoM,'dir') % Run Centre of Mass analysis
                Lb2.f_setup_run_CoM();
            end
            
      
        end
    end
end

%%
if 0 % Plots
    Lb2.f_GUI_c3d;
    Lb2.f_GUI_mot;
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(0,0); %F
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
    Lb2.f_fig_subplot_res_Knee_child(0);
    Lb2.f_fig_subplot_T_interest_ID(0);
    %     Lb2.f_fig_subplot_res_OpenSim_SO_a(0,0);
end
