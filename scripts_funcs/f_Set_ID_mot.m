function [ IDtool ] = f_Set_ID_mot( IDtool, mot )
%UNTITLED Summary of this function goes here
%   IDtool.setCoordinatesFileName(motionfilename) can't update time
IDtool.setCoordinatesFileName(mot);
a=importdata(mot);
IDtool.setStartTime(a.data(1,1));
IDtool.setEndTime(a.data(end,1));

end
