This is a pipeline for batch processing - Vicon C3D -> OpenSim model scaling -> inverse kinematic -> centre of mass analysis, though each step can also be performed within OpenSim GUI. I would recommend getting started with GUI first, and then use this pipeline.


To use this pipeline, please configure the Matlab-OpenSim API following 
https://simtk-confluence.stanford.edu:8443/display/OpenSim/Scripting+with+Matlab

Use PG_201_scale.m for model scaling.
Use PG_202_RunBioMch_SO_EmgInformed for biomech simulations, which is unnecessary for the centre of mass analysis, but feel free to explore.
Use PG_203_CentreOfMass for inverse kinematic and centre of mass analyses.

Put c3d files in \data_lfs\c3d. I left some c3d sample data there to help get started.

You might need to alter the .osim file to reflect the body mass changes due to the backpack.
https://simtk-confluence.stanford.edu:8443/display/OpenSim/Model+Editing

After you have finalised the Vicon markerset, you might need to alter the Scaling and Inverse-kinematics parts of the pipeline. 
For more information on Scaling:
https://simtk-confluence.stanford.edu:8443/display/OpenSim/Scaling

For more information on Inverse-kinematics:
https://simtk-confluence.stanford.edu:8443/display/OpenSim/Inverse+Kinematics

Might need to download some dependent functions from this repo
https://gitlab.com/melb.ww/10_my_matlab_func

Good luck and have fun!